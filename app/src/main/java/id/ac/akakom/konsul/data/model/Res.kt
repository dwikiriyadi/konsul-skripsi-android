package id.ac.akakom.konsul.data.model

import com.google.gson.annotations.SerializedName

data class Res<T>(
    @SerializedName("messages") val messages: String,
    @SerializedName("result") val result: T
)