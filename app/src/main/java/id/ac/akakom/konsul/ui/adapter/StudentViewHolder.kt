package id.ac.akakom.konsul.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import id.ac.akakom.konsul.data.model.Student
import id.ac.akakom.konsul.databinding.StudentItemBinding

class StudentViewHolder(val binding: StudentItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Student) {
        binding.data = item
    }

}