package id.ac.akakom.konsul.data

import id.ac.akakom.konsul.data.model.Guidance
import id.ac.akakom.konsul.data.model.Res
import id.ac.akakom.konsul.data.model.Student
import id.ac.akakom.konsul.data.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

fun login(
    username: String,
    password: String,
    onSuccess: (body: Res<String>) -> Unit,
    onFail: (response: Response<Res<String>>) -> Unit
) {
    API.create().postLogin(username, password).enqueue(object : Callback<Res<String>> {
        override fun onFailure(call: Call<Res<String>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<String>>, response: Response<Res<String>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}

fun logout(
    onSuccess: (body: Res<String>) -> Unit,
    onFail: (response: Response<Res<String>>) -> Unit,
    token: String
) {
    API.create().postLogout(token).enqueue(object : Callback<Res<String>> {
        override fun onFailure(call: Call<Res<String>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<String>>, response: Response<Res<String>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}

fun getUser(
    onSuccess: (body: Res<User>) -> Unit,
    onFail: (response: Response<Res<User>>) -> Unit,
    token: String
) {
    API.create().getUser(token).enqueue(object : Callback<Res<User>> {
        override fun onFailure(call: Call<Res<User>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<User>>, response: Response<Res<User>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}

fun getStudents(
    onSuccess: (body: Res<ArrayList<Student>>) -> Unit,
    onFail: (response: Response<Res<ArrayList<Student>>>) -> Unit,
    token: String
) {
    API.create().getStudents(token).enqueue(object : Callback<Res<ArrayList<Student>>> {
        override fun onFailure(call: Call<Res<ArrayList<Student>>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<ArrayList<Student>>>, response: Response<Res<ArrayList<Student>>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}

fun getGuidances(
    token: String,
    student_id: Int,
    onSuccess: (body: Res<ArrayList<Guidance>>) -> Unit,
    onFail: (response: Response<Res<ArrayList<Guidance>>>) -> Unit
) {
    API.create().getGuidances(token, student_id).enqueue(object : Callback<Res<ArrayList<Guidance>>> {
        override fun onFailure(call: Call<Res<ArrayList<Guidance>>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<ArrayList<Guidance>>>, response: Response<Res<ArrayList<Guidance>>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}

fun storeGuidance(
    token: String, item: Guidance,
    onSuccess: (body: Res<Any>) -> Unit,
    onFail: (response: Response<Res<Any>>) -> Unit
) {
    API.create().storeGuidance(token, item).enqueue(object : Callback<Res<Any>> {
        override fun onFailure(call: Call<Res<Any>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<Any>>, response: Response<Res<Any>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}

fun updateGuidance(
    token: String,
    id: Int,
    item: Guidance,
    onSuccess: (body: Res<Guidance>) -> Unit,
    onFail: (response: Response<Res<Guidance>>) -> Unit
) {
    API.create().updateGuidance(token, id, item).enqueue(object : Callback<Res<Guidance>> {
        override fun onFailure(call: Call<Res<Guidance>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<Guidance>>, response: Response<Res<Guidance>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}

fun deleteGuidance(
    token: String,
    id: Int,
    onSuccess: (body: Res<Guidance>) -> Unit,
    onFail: (response: Response<Res<Guidance>>) -> Unit
) {
    API.create().deleteGuidance(token, id).enqueue(object : Callback<Res<Guidance>> {
        override fun onFailure(call: Call<Res<Guidance>>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<Res<Guidance>>, response: Response<Res<Guidance>>) {
            if (response.isSuccessful) {
                onSuccess(response.body()!!)
            } else {
                onFail(response)
            }
        }
    })
}