package id.ac.akakom.konsul.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import id.ac.akakom.konsul.R
import id.ac.akakom.konsul.data.logout
import id.ac.akakom.konsul.data.model.Guidance
import id.ac.akakom.konsul.data.model.Student
import id.ac.akakom.konsul.data.model.User
import id.ac.akakom.konsul.databinding.GuidanceItemBinding
import id.ac.akakom.konsul.databinding.StudentItemBinding
import id.ac.akakom.konsul.databinding.UserItemBinding
import id.ac.akakom.konsul.utility.SharedPrefManager
import id.ac.akakom.konsul.utility.toast
import org.json.JSONObject

class MainAdapter(val data: ArrayList<Any>, val onClickListener: OnClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnClickListener {
        fun onClick(item: Any)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_USER -> {
                UserViewHolder(UserItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            }
            TYPE_STUDENT -> {
                StudentViewHolder(StudentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            }
            else -> {
                GuidanceViewHolder(GuidanceItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            }
        }
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]
        when (getItemViewType(position)) {
            TYPE_USER -> {
               (holder as UserViewHolder).apply {
                   binding.apply {
                       signOut.setOnClickListener {
                           onClickListener.onClick(item)
                       }
                   }
                   bind(item as User)
               }
            }
            TYPE_STUDENT -> {
                (holder as StudentViewHolder).apply {
                    binding.root.setOnClickListener {
                        onClickListener.onClick(item)
                    }
                    bind(item as Student)
                }

            }
            else -> {
                (holder as GuidanceViewHolder).apply {
                    binding.root.setOnClickListener {
                        onClickListener.onClick(item)
                    }
                    bind(item as Guidance)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is User -> {
                TYPE_USER
            }
            is Student -> {
                TYPE_STUDENT
            }
            else -> {
                TYPE_GUIDANCE
            }
        }
    }

    companion object {
        private const val TYPE_USER = 1
        private const val TYPE_STUDENT = 2
        private const val TYPE_GUIDANCE = 3
    }
}