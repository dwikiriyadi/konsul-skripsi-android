package id.ac.akakom.konsul.ui.fragment.guidance

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.ac.akakom.konsul.R
import id.ac.akakom.konsul.data.getGuidances
import id.ac.akakom.konsul.data.logout
import id.ac.akakom.konsul.data.model.Guidance
import id.ac.akakom.konsul.data.model.Student
import id.ac.akakom.konsul.data.model.User
import id.ac.akakom.konsul.databinding.LayoutRecyclerviewBinding
import id.ac.akakom.konsul.ui.MainViewModel
import id.ac.akakom.konsul.ui.adapter.MainAdapter
import id.ac.akakom.konsul.utility.MarginItemDecoration
import id.ac.akakom.konsul.utility.SharedPrefManager
import id.ac.akakom.konsul.utility.toast
import org.json.JSONObject

class GuidanceListFragment : Fragment() {

    private lateinit var binding: LayoutRecyclerviewBinding
    private lateinit var viewModel: GuidanceListViewModel
    private lateinit var mainViewModel: MainViewModel
    private val sharedPrefManager by lazy { SharedPrefManager(context!!) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutRecyclerviewBinding.inflate(inflater, container, false)

        viewModel = ViewModelProviders.of(this).get(GuidanceListViewModel::class.java)
        mainViewModel = activity?.run { ViewModelProviders.of(this).get(MainViewModel::class.java) }
            ?: throw Exception("Invalid Activity")

        binding.recyclerview.apply {
            layoutManager = LinearLayoutManager(context!!)
            addItemDecoration(MarginItemDecoration(8))
        }
        viewModel.data.clear()

        mainViewModel.user.observe(this, Observer {
            binding.swipeRefresh.setOnRefreshListener {
                viewModel.data.clear()
                if (it.role == "student") viewModel.data.add(0, it)
                getGuidances(if (it.role == "student") it.id else mainViewModel.student.value!!.id)
            }

            if (it.role == "student") {
                mainViewModel.title.postValue("Guidance")

                viewModel.data.add(0, it)
                //request guidances
                getGuidances(it.id)

                binding.addButton.apply {
                    show()
                    setOnClickListener {
                        findNavController().navigate(R.id.action_guidanceListFragment_to_guidanceFragment, bundleOf("fragment" to "add"))
                    }
                }
            } else {
                mainViewModel.title.postValue(mainViewModel.student.value!!.name)
                getGuidances(mainViewModel.student.value!!.id)

                binding.addButton.hide()
            }
        })

        mainViewModel.data.observe(this, Observer {
            binding.recyclerview.adapter = MainAdapter(it, object : MainAdapter.OnClickListener {
                override fun onClick(item: Any) {
                    when (item) {
                        is User -> {
                            logout({ body ->
                                sharedPrefManager.isLogin = null
                                mainViewModel.data.value!!.clear()
                                context!!.toast(body.messages)
                                activity!!.findNavController(R.id.activity_host_fragment)
                                    .navigate(R.id.action_mainFragment_to_signInFragment)
                            }, { response ->
                                val body = JSONObject(response.errorBody()!!.string())
                                when (response.code()) {
                                    404, 401 -> {
                                        context!!.toast(body.getString("message"))
                                        activity!!.findNavController(R.id.activity_host_fragment)
                                            .navigate(R.id.action_mainFragment_to_signInFragment)
                                    }
                                }
                            }, sharedPrefManager.isLogin!!)
                        }
                        else -> {
                            val guidance = item as Guidance
                            if (guidance.status == 0) {
                                mainViewModel.guidance.postValue(guidance)
                                findNavController().navigate(R.id.action_guidanceListFragment_to_guidanceFragment, bundleOf("fragment" to "edit"))
                            } else {
                                context!!.toast("This guidance has been approved")
                            }
                        }
                    }
                }
            })
        })

        return binding.root
    }

    private fun getGuidances(student_id: Int) {
        binding.swipeRefresh.isRefreshing = true

        getGuidances(SharedPrefManager(context!!).isLogin!!, student_id, { body ->
            binding.swipeRefresh.isRefreshing = false
            viewModel.data.addAll(body.result)
            mainViewModel.data.postValue(viewModel.data)
        }, { response ->
            val body = JSONObject(response.errorBody()!!.string())
            when (response.code()) {
                404, 401 -> {
                    binding.swipeRefresh.isRefreshing = false
                    context!!.toast(body.getString("message"))
                    activity!!.findNavController(R.id.activity_host_fragment)
                        .navigate(R.id.action_mainFragment_to_signInFragment)
                }
            }
        })
    }
}
