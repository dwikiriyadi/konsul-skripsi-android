package id.ac.akakom.konsul.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import id.ac.akakom.konsul.data.model.User
import id.ac.akakom.konsul.databinding.UserItemBinding

class UserViewHolder(val binding: UserItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: User) {
        binding.data = item
    }
}