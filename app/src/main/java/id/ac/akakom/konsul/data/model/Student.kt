package id.ac.akakom.konsul.data.model

import com.google.gson.annotations.SerializedName

data class Student(
    @SerializedName("id") val id: Int,
    @SerializedName("id_number") val id_number: String,
    @SerializedName("name") val name: String,
    @SerializedName("image_url") val imageUrl: String
)