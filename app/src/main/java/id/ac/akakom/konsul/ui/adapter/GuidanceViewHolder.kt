package id.ac.akakom.konsul.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import id.ac.akakom.konsul.data.model.Guidance
import id.ac.akakom.konsul.databinding.GuidanceItemBinding

class GuidanceViewHolder(val binding: GuidanceItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Guidance) {
        binding.data = item
    }

}