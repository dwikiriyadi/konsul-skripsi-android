package id.ac.akakom.konsul.ui.fragment.guidance

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.ac.akakom.konsul.R
import id.ac.akakom.konsul.data.*
import id.ac.akakom.konsul.databinding.GuidanceFragmentBinding
import id.ac.akakom.konsul.ui.MainViewModel
import id.ac.akakom.konsul.utility.*
import org.json.JSONObject
import id.ac.akakom.konsul.data.model.*

class GuidanceFragment : Fragment() {

    private lateinit var binding: GuidanceFragmentBinding
    private lateinit var mainViewModel: MainViewModel
    private val navController by lazy { findNavController() }
    private val sharedPrefManager by lazy { SharedPrefManager(context!!) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = GuidanceFragmentBinding.inflate(inflater, container, false)

        setHasOptionsMenu(true)

        mainViewModel = activity?.run { ViewModelProviders.of(this).get(MainViewModel::class.java) }
            ?: throw Exception("Invalid Activity")

        when (arguments!!["fragment"]) {
            "add" -> {
                binding.title.isEnabled = true
                binding.body.isEnabled = true

                binding.title.setText("Untitled")
                binding.body.setText("")
            }
            else -> {
                mainViewModel.guidance.observe(this, Observer {
                    if (mainViewModel.user.value!!.role == "student") {
                        binding.title.isEnabled = true
                        binding.body.isEnabled = true
                    } else {
                        binding.title.isEnabled = false
                        binding.body.isEnabled = false
                    }

                    binding.title.setText(it!!.title)
                    binding.body.setText(it.body)
                })
            }
        }
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_guidance, menu)
        menu.apply {
            menu.findItem(R.id.menu_delete).isVisible = mainViewModel.user.value!!.role == "student"
            menu.findItem(R.id.menu_save).setTitle(
                if (mainViewModel.user.value!!.role == "student") R.string.save else R.string.approve
            )
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_save -> {
                onSaveOrApprove()
                true
            }
            R.id.menu_delete -> {
                onDelete()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onDelete() {
        deleteGuidance(
            sharedPrefManager.isLogin!!,
            mainViewModel.guidance.value!!.id!!,
            { body ->
                context!!.toast(body.messages)
                navController.navigate(R.id.action_guidanceFragment_to_guidanceListFragment)
            },
            { response ->
                val body = JSONObject(response.errorBody()!!.string())
                when (response.code()) {
                    404, 401 -> {
                        context!!.toast(body.getString("message"))
                        activity!!.findNavController(R.id.activity_host_fragment)
                            .navigate(R.id.action_mainFragment_to_signInFragment)
                    }
                }
            }
        )
    }

    private fun onSaveOrApprove() {
        when (arguments!!["fragment"]) {
            "add" -> onAdd()
            else -> onEdit()
        }
    }

    private fun onAdd() {
        val guidance = Guidance(title = binding.title.text.toString(), body = binding.body.text.toString())

        storeGuidance(sharedPrefManager.isLogin!!,
            guidance,
            {body ->
                context!!.toast(body.messages)
                navController.navigate(R.id.action_guidanceFragment_to_guidanceListFragment)
            },
            { response ->
                val body = JSONObject(response.errorBody()!!.string())
                when (response.code()) {
                    404, 401 -> {
                        context!!.toast(body.getString("message"))
                        activity!!.findNavController(R.id.activity_host_fragment)
                            .navigate(R.id.action_mainFragment_to_signInFragment)
                    }
                }
            })
    }

    private fun onEdit() {
        when (mainViewModel.user.value!!.role) {
            "student" -> {
                mainViewModel.guidance.value!!.title = binding.title.text.toString()
                mainViewModel.guidance.value!!.body = binding.body.text.toString()
                update()
            }
            else -> {
                mainViewModel.guidance.value!!.status = 1
                update()
            }
        }
    }

    private fun update() {
        updateGuidance(
            sharedPrefManager.isLogin!!,
            mainViewModel.guidance.value!!.id!!,
            mainViewModel.guidance.value!!,
            {body ->
                context!!.toast(body.messages)
                navController.navigate(R.id.action_guidanceFragment_to_guidanceListFragment)
            },
            { response ->
                val body = JSONObject(response.errorBody()!!.string())
                when (response.code()) {
                    404, 401 -> {
                        context!!.toast(body.getString("message"))
                        activity!!.findNavController(R.id.activity_host_fragment)
                            .navigate(R.id.action_mainFragment_to_signInFragment)
                    }
                }
            }
        )
    }
}
