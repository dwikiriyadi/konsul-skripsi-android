package id.ac.akakom.konsul.data

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

import id.ac.akakom.konsul.data.model.*

interface API {

    @FormUrlEncoded
    @POST("signin")
    fun postLogin(@Field("username") username: String, @Field("password") password: String): Call<Res<String>>

    @POST("signout")
    fun postLogout(@Header("Authorization") token: String): Call<Res<String>>

    @GET("user")
    fun getUser(@Header("Authorization") token: String): Call<Res<User>>

    @GET("student")
    fun getStudents(@Header("Authorization") token: String): Call<Res<ArrayList<Student>>>

    @GET("guidance")
    fun getGuidances(@Header("Authorization") token: String, @Query("student_id") id: Int? = null): Call<Res<ArrayList<Guidance>>>

    @GET("guidance/{id}")
    fun getGuidance(@Header("Authorization") token: String, @Path("id") id: Int): Call<Res<Guidance>>

    @POST("guidance")
    fun storeGuidance(@Header("Authorization") token: String, @Body guidance: Guidance): Call<Res<Any>>

    @POST("guidance/{id}")
    fun updateGuidance(@Header("Authorization") token: String, @Path("id") id: Int, @Body guidance: Guidance): Call<Res<Guidance>>

    @DELETE("guidance/{id}")
    fun deleteGuidance(@Header("Authorization") token: String, @Path("id") id: Int): Call<Res<Guidance>>

    companion object {
        private const val URL = "http://192.168.137.1:8000/"

        fun create(): API = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(URL)
            .build()
            .create(API::class.java)
    }
}