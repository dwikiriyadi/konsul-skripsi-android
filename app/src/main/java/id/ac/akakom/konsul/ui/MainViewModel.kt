package id.ac.akakom.konsul.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.ac.akakom.konsul.data.model.Guidance
import id.ac.akakom.konsul.data.model.Student
import id.ac.akakom.konsul.data.model.User

class MainViewModel : ViewModel() {
    val title = MutableLiveData<String>()
    val user = MutableLiveData<User>()
    val student = MutableLiveData<Student?>()
    val guidance = MutableLiveData<Guidance?>()
    val data = MutableLiveData<ArrayList<Any>>()
}
