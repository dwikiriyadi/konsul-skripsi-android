package id.ac.akakom.konsul.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import id.ac.akakom.konsul.R
import id.ac.akakom.konsul.data.login
import id.ac.akakom.konsul.databinding.SignInFragmentBinding
import id.ac.akakom.konsul.utility.*
import org.json.JSONObject


class SignInFragment : Fragment() {

    private lateinit var binding: SignInFragmentBinding
    private val navController by lazy { this.findNavController() }
    private val sharedPrefManager by lazy { SharedPrefManager(context!!) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SignInFragmentBinding.inflate(inflater, container, false)

        binding.loginButton.setOnClickListener {
            login(
                binding.username.text.toString(),
                binding.password.text.toString(),
                { body ->
                    sharedPrefManager.isLogin = body.result
                    navController.navigate(R.id.action_signInFragment_to_mainFragment)
                }, { response ->
                    val body = JSONObject(response.errorBody()!!.string())
                    when (response.code()) {
                        404, 401 -> context!!.toast(body.getString("message"))
                    }
                })
        }

        return binding.root
    }
}
