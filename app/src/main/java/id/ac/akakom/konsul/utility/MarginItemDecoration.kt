package id.ac.akakom.konsul.utility

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt

class MarginItemDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

//        if (parent.getChildAdapterPosition(view) == 0) {
//            outRect.top = (spaceHeight * Resources.getSystem().displayMetrics.density).roundToInt()
//        }
        outRect.bottom = (spaceHeight * Resources.getSystem().displayMetrics.density).roundToInt()

    }
}