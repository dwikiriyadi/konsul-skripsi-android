package id.ac.akakom.konsul.data.model

import com.google.gson.annotations.SerializedName

data class Guidance(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("title") var title: String,
    @SerializedName("body") var body: String,
    @SerializedName("status") var status: Int? = null,
    @SerializedName("created_at") val created_at: String? = null
)