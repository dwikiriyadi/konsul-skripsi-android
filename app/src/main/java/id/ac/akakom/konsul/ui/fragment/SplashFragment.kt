package id.ac.akakom.konsul.ui.fragment

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import id.ac.akakom.konsul.databinding.SplashFragmentBinding
import id.ac.akakom.konsul.utility.*
import id.ac.akakom.konsul.R

class SplashFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = SplashFragmentBinding.inflate(inflater,container,false)

        val sharedPrefManager = SharedPrefManager(context!!)

        val navController = this.findNavController()

        Handler().postDelayed({
            if (sharedPrefManager.isLogin.isNullOrEmpty()) {
                navController.navigate(R.id.action_splashFragment_to_signInFragment)
            } else {
                navController.navigate(R.id.action_splashFragment_to_mainFragment)
            }
        },3000)


        return binding.root
    }

}
