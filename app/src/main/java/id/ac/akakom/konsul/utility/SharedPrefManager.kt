package id.ac.akakom.konsul.utility

import android.content.Context

class SharedPrefManager(private val context: Context) {

    private val sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
    private val editor = sharedPreferences.edit()

    var isLogin: String?
        set(value) {
            editor.putString(KEY_IS_LOGIN, value).commit()
        }
        get() = sharedPreferences.getString(KEY_IS_LOGIN, null)

    companion object {
        private const val SHARED_PREF_NAME = "konsul"
        private const val KEY_IS_LOGIN = "is_login"
    }
}
