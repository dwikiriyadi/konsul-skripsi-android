package id.ac.akakom.konsul.ui.fragment.lecturer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.ac.akakom.konsul.R
import id.ac.akakom.konsul.data.*
import id.ac.akakom.konsul.data.model.*
import id.ac.akakom.konsul.databinding.LayoutRecyclerviewBinding
import id.ac.akakom.konsul.ui.MainViewModel
import id.ac.akakom.konsul.ui.adapter.MainAdapter
import id.ac.akakom.konsul.utility.*
import org.json.JSONObject

class LecturerFragment : Fragment() {

    private lateinit var binding: LayoutRecyclerviewBinding
    private lateinit var viewModel: LecturerViewModel
    private lateinit var mainViewModel: MainViewModel
    private val sharedPrefManager by lazy { SharedPrefManager(context!!) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutRecyclerviewBinding.inflate(inflater, container, false)

        viewModel = ViewModelProviders.of(this).get(LecturerViewModel::class.java)
        mainViewModel = activity?.run { ViewModelProviders.of(this).get(MainViewModel::class.java) }
            ?: throw Exception("Invalid Activity")

        mainViewModel.title.postValue("")

        viewModel.data.clear()

        binding.recyclerview.apply {
            layoutManager = LinearLayoutManager(context!!)
            addItemDecoration(MarginItemDecoration(8))
        }

        mainViewModel.user.observe(this, Observer {
            binding.swipeRefresh.apply {
                setOnRefreshListener {
                    viewModel.data.clear()
                    viewModel.data.add(0, it)
                    getStudents()
                }
            }

            viewModel.data.add(0, it)
        })

        // request student
        getStudents()

        mainViewModel.data.postValue(viewModel.data)

        mainViewModel.data.observe(this, Observer {
            binding.recyclerview.adapter = MainAdapter(it, object : MainAdapter.OnClickListener {
                override fun onClick(item: Any) {
                    when (item) {
                        is User -> {
                            logout({ body ->
                                sharedPrefManager.isLogin = null
                                mainViewModel.data.value!!.clear()
                                context!!.toast(body.messages)
                                activity!!.findNavController(R.id.activity_host_fragment)
                                    .navigate(R.id.action_mainFragment_to_signInFragment)
                            }, { response ->
                                val body = JSONObject(response.errorBody()!!.string())
                                when (response.code()) {
                                    404, 401 -> {
                                        context!!.toast(body.getString("message"))
                                        activity!!.findNavController(R.id.activity_host_fragment)
                                            .navigate(R.id.action_mainFragment_to_signInFragment)
                                    }
                                }
                            }, sharedPrefManager.isLogin!!)
                        }
                        else -> {
                            mainViewModel.student.postValue(item as Student)
                            findNavController().navigate(R.id.action_lecturerFragment_to_guidanceListFragment)
                        }
                    }
                }
            })
        })

        return binding.root
    }

    private fun getStudents() {
        binding.swipeRefresh.isRefreshing = true

        getStudents({body ->
            binding.swipeRefresh.isRefreshing = false
            viewModel.data.addAll(body.result)
            mainViewModel.data.postValue(viewModel.data)
        }, {response ->
            val body = JSONObject(response.errorBody()!!.string())
            when (response.code()) {
                404, 401 -> {
                    binding.swipeRefresh.isRefreshing = false
                    context!!.toast(body.getString("message"))
                    activity!!.findNavController(R.id.activity_host_fragment)
                        .navigate(R.id.action_mainFragment_to_signInFragment)
                }
            }
        }, sharedPrefManager.isLogin!!)
    }
}
