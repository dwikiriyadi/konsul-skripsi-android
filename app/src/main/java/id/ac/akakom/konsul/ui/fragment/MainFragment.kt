package id.ac.akakom.konsul.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import id.ac.akakom.konsul.R
import id.ac.akakom.konsul.data.getUser
import id.ac.akakom.konsul.databinding.MainFragmentBinding
import id.ac.akakom.konsul.ui.MainViewModel
import id.ac.akakom.konsul.utility.SharedPrefManager
import id.ac.akakom.konsul.utility.toast
import org.json.JSONObject

class MainFragment : Fragment() {

    private lateinit var binding: MainFragmentBinding
    private lateinit var viewModel: MainViewModel
    private val sharedPrefManager by lazy { SharedPrefManager(context!!) }
    private val navHostFragment by lazy { childFragmentManager.findFragmentById(R.id.main_host_fragment) as NavHostFragment }
    private val mainActivity by lazy { (activity!! as AppCompatActivity) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(inflater, container, false)

        viewModel = activity?.run { ViewModelProviders.of(this).get(MainViewModel::class.java) }
            ?: throw Exception("Invalid Activity")

        val navInflater = navHostFragment.navController.navInflater

        val graph = navInflater.inflate(R.navigation.navigation_main)

        mainActivity.setSupportActionBar(binding.toolbar)

        val callback = mainActivity.onBackPressedDispatcher.addCallback(this) {
            navHostFragment.navController.navigateUp()
        }

        callback.isEnabled

        viewModel.title.observe(this, Observer {
            binding.toolbar.title = it
        })

        getUser({ body ->
            viewModel.user.postValue(body.result)
            viewModel.student.postValue(null)
            graph.apply {
                startDestination = if (body.result.role == "student") {
                    R.id.guidanceListFragment
                } else {
                    R.id.lecturerFragment
                }
            }
            navHostFragment.navController.graph = graph
        }, { response ->
            val body = JSONObject(response.errorBody()!!.string())
            when (response.code()) {
                404, 401 -> {
                    context!!.toast(body.getString("message"))
                    this.findNavController().navigate(R.id.action_mainFragment_to_signInFragment)
                }
            }
        }, sharedPrefManager.isLogin!!)


        return binding.root
    }
}
